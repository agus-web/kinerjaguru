<?php
session_start();
include "include/notice.php";
include "include/koneksi.php";
include "include/fungsi.php";
$page=$_REQUEST["page"];
$id_lokasi=$_SESSION["ses_idlokasi"];
?>

<head>
	<title>Penilaian Kinerja Guru</title>
	<link href="images/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="images/footer.css" rel="stylesheet" type="text/css" media="all" />
	<link href="images/home.css" rel="stylesheet" type="text/css" media="all" />
	<link href="images/bootsrap.css" rel="stylesheet" type="text/css" media="all" />
	
	
</head>

<body class="home">
	<div id="bg">
		<div id="page">
			<img src="images/logo.png" />
			<div id="body_content">
				<table width="100%"  border="0" cellspacing="0" cellpadding="0" style="margin-left:-11px">
					<tr>
						<td valign="top">
							<?php if(!empty($_SESSION["ses_login"])){?>
								<div id="slider_menu"><h2 class="title" id="menu_utama"> Menu Admin </h2>
									<ul id="menu_utama_ul">
										<li><a href="./">Halaman Depan</a></li>
										<?php if($_SESSION["ses_level"]=="Admin"){?>
											<li><a href="?page=p_user">User</a></li>
											<li><a href="?page=p_alternatif">Data Alternatif</a></li>
											<li><a href="?page=p_kriteria">Data Kriteria</a></li>
											<li><a href="?page=p_himpunan">Himpunan</a></li>
											<li><a href="?page=p_klasifikasi">Klasifikasi Guru</a></li>
											<li><a href="?page=p_analisa">Seleksi</a></li>
											<li><a href="?page=p_hasil">Hasil</a></li>
											<?php }else if($_SESSION["ses_level"]=="Dosen"){?>
												<li><a href="?page=p_hasil">Hasil</a></li>	
												<?php }?>
												<li><a href="logout.php">Logout</a></li>
											</ul>
										</div>
										<?php }else{?>
											<div id="slider_menu">
												<ul id="menu_pencarian_ul">
													<li><a href="./">Halaman Depan</a></li>
													<li><a href="?page=p_login">Login</a></li>
												</ul>
											</div>
											<?php }?>
											<div class="spacer_float"></div>
										</td>
										<td valign="top" width="660"><script language="javascript">
											function DeleteConfirm(url){
												if (confirm("Apakah anda yakin ingin menghapus ?")){
													window.location.href=url;
												}
											}
										</script>

										<div style="font-family:Arial;font-size:12px;padding:3px ">
											<div style="font-size:24px;padding:10px;padding-left:0px;">Metode Simple Additive Weighting</div>
											<div style="height:20px;"></div>                                                 <?php include "include/bukaprogram.php"; ?>

											<br />
										</div>
									</td>
								</tr>
							</table>

							
						</div>
						
						<div id="body_footer">
							<div id="footer_links">
								Copyright &copy; Ajat Nurajat 2018. </a>
							</div>
							<div class="clear"></div></div>

						</div></div>
						</html>
