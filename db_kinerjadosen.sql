-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 02, 2015 at 01:20 AM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db_kinerjadosen`
--

-- --------------------------------------------------------

--
-- Table structure for table `alternatif`
--

CREATE TABLE IF NOT EXISTS `alternatif` (
  `id_alternatif` int(11) NOT NULL AUTO_INCREMENT,
  `nidn` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `nm_dosen` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `status` varchar(10) COLLATE latin1_general_ci NOT NULL,
  `jabatan` varchar(255) COLLATE latin1_general_ci NOT NULL,
  `no_telp` varchar(15) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_alternatif`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=36 ;

--
-- Dumping data for table `alternatif`
--

INSERT INTO `alternatif` (`id_alternatif`, `nidn`, `nm_dosen`, `status`, `jabatan`, `no_telp`) VALUES
(29, '411700435', 'Sofi Defiyanti, S.Kom., M.Kom', 'Tetap YYS', 'Dosen', '083899671007'),
(30, '411700508', 'Mohamad Jajuli, S.Si., M.Si', 'Tetap YYS', 'Dosen', '085722022070'),
(31, '411700589', 'Hanny Hikmayanti, S.Kom., M.Kom', 'Tetap YYS', 'Dosen', '085720304821'),
(35, '(none)', 'Ahmad Fauzi', 'Tetap YYS', 'Dosen', '081281702393'),
(33, '411700434', 'Azhari Ali Ridha, S.Kom', 'Tetap YYS', 'Dosen', '08129341022');

-- --------------------------------------------------------

--
-- Table structure for table `himpunan`
--

CREATE TABLE IF NOT EXISTS `himpunan` (
  `id_himpunan` int(11) NOT NULL AUTO_INCREMENT,
  `id_kriteria` int(11) NOT NULL,
  `nm_himpunan` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `nilai` float NOT NULL,
  PRIMARY KEY (`id_himpunan`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=132 ;

--
-- Dumping data for table `himpunan`
--

INSERT INTO `himpunan` (`id_himpunan`, `id_kriteria`, `nm_himpunan`, `nilai`) VALUES
(1, 1, 'X <= 750.000', 10),
(2, 1, '750.000 < X <= 1.500.000', 7.5),
(3, 1, '1.500.000 < X < 2.250.000', 5),
(85, 25, 'Sarjana', 3),
(5, 2, '1', 4.425),
(6, 2, '2', 4.455),
(7, 2, '3', 4.475),
(8, 2, '4', 4.5),
(9, 3, 'Ya', 2.5),
(10, 3, 'Tidak', 0),
(11, 4, 'Nilai <= 70', 2.7),
(12, 4, '70 < Nilai <= 80', 2.725),
(13, 4, '80 < Nilai <= 95', 2.75),
(14, 4, 'Nilai > 95', 2.775),
(17, 5, 'Tidak', 2),
(16, 5, 'Ya', 0),
(53, 6, 'MIA', 1),
(54, 6, 'IIS', 0.5),
(55, 7, 'MIA', 1),
(56, 7, 'IIS', 0.5),
(57, 8, 'MIA', 1),
(58, 8, 'IIS', 0.5),
(59, 9, 'X <= 70', 0.25),
(60, 9, '70 < nilai <= 80', 0.5),
(61, 9, '80 < nilai <= 95', 0.75),
(62, 9, 'nilai > 95', 1),
(63, 10, 'X <= 70', 0.25),
(64, 10, '70 < nilai <= 80', 0.5),
(65, 10, '80 < nilai <= 95', 0.75),
(66, 10, 'nilai > 95', 1),
(67, 14, 'Sangat puas', 90),
(68, 14, 'Puas', 70),
(69, 14, 'Cukup', 50),
(70, 14, 'Kurang Puas', 30),
(71, 16, 'Ya', 100),
(72, 16, 'Tidak', 0),
(73, 17, 'Ya', 100),
(74, 17, 'Tidak', 0),
(75, 18, 'Ya', 100),
(76, 18, 'Tidak', 0),
(77, 19, 'S1', 30),
(78, 19, 'S2', 60),
(79, 19, 'S3', 90),
(80, 20, 'Ya', 100),
(81, 20, 'Tidak', 0),
(82, 21, 'Ya', 100),
(83, 21, 'Tidak', 0),
(86, 25, 'Magister', 6),
(87, 25, 'Doktor', 9),
(88, 26, 'Ya', 10),
(89, 26, 'Tidak', 0),
(90, 27, 'Ya', 10),
(91, 27, 'Tidak', 0),
(92, 28, 'Ya', 10),
(93, 29, 'Sangat Baik', 10),
(94, 29, 'Tidak Baik', 0),
(95, 28, 'Tidak', 0),
(96, 30, 'Sangat Baik', 10),
(97, 30, 'Tidak Baik', 0),
(98, 31, '8 < Absensi < 14', 7),
(99, 31, '3 < Absensi <=8', 4),
(100, 31, 'Absensi = 14', 10),
(101, 32, 'Sangat puas', 10),
(102, 32, 'Puas', 7.5),
(103, 32, 'Cukup', 5),
(104, 32, 'Kurang Puas', 2.5),
(105, 32, 'Tidak puas', 0),
(106, 33, 'Sangat Baik', 10),
(107, 33, 'Baik', 7.5),
(108, 33, 'Cukup', 5),
(109, 33, 'Kurang Baik', 25),
(110, 33, 'Tidak Baik', 0),
(111, 34, 'Sangat Baik', 10),
(112, 34, 'Baik', 7.5),
(113, 34, 'Cukup', 5),
(114, 34, 'Kurang Baik', 2.5),
(115, 34, 'Tidak Baik', 0),
(116, 29, 'Cukup', 5),
(117, 29, 'Baik', 7.5),
(118, 29, 'Kurang Baik', 2.5),
(119, 30, 'Cukup', 5),
(120, 30, 'Kurang Baik', 2.5),
(121, 30, 'Baik', 7.5),
(122, 35, 'Ya', 10),
(123, 35, 'Tidak', 0),
(124, 36, 'Ya', 10),
(125, 36, 'Tidak', 0),
(126, 37, 'Ya', 10),
(127, 37, 'Tidak', 0),
(128, 38, 'Ya', 10),
(129, 38, 'Tidak', 0),
(130, 39, 'Ya', 10),
(131, 39, 'Tidak', 0);

-- --------------------------------------------------------

--
-- Table structure for table `klasifikasi`
--

CREATE TABLE IF NOT EXISTS `klasifikasi` (
  `id_alternatif` int(11) NOT NULL,
  `id_himpunan` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Dumping data for table `klasifikasi`
--

INSERT INTO `klasifikasi` (`id_alternatif`, `id_himpunan`) VALUES
(1, 13),
(1, 8),
(1, 4),
(2, 57),
(2, 53),
(2, 55),
(3, 14),
(3, 8),
(3, 4),
(4, 63),
(4, 61),
(4, 58),
(6, 58),
(6, 54),
(6, 56),
(7, 3),
(7, 8),
(7, 14),
(5, 2),
(5, 5),
(5, 12),
(8, 2),
(8, 8),
(8, 14),
(10, 2),
(10, 8),
(10, 12),
(4, 54),
(4, 55),
(2, 59),
(2, 63),
(6, 61),
(6, 65),
(16, 75),
(16, 73),
(16, 71),
(16, 67),
(16, 78),
(16, 80),
(16, 83),
(17, 76),
(17, 73),
(17, 71),
(17, 68),
(17, 79),
(17, 80),
(17, 82),
(31, 98),
(31, 117),
(31, 121),
(31, 112),
(31, 95),
(31, 86),
(31, 88),
(31, 91),
(31, 102),
(31, 107),
(31, 123),
(31, 124),
(31, 126),
(31, 128),
(31, 130),
(33, 98),
(33, 117),
(33, 119),
(33, 112),
(33, 95),
(33, 85),
(33, 88),
(33, 91),
(33, 102),
(33, 107),
(33, 123),
(33, 125),
(33, 127),
(33, 129),
(33, 131),
(30, 98),
(30, 117),
(30, 121),
(30, 112),
(30, 95),
(30, 86),
(30, 88),
(30, 91),
(30, 102),
(30, 107),
(30, 122),
(30, 125),
(30, 127),
(30, 128),
(30, 130),
(29, 98),
(29, 117),
(29, 121),
(29, 112),
(29, 95),
(29, 86),
(29, 88),
(29, 91),
(29, 102),
(29, 107),
(29, 123),
(29, 124),
(29, 127),
(29, 128),
(29, 130),
(28, 100),
(28, 117),
(28, 119),
(28, 112),
(28, 95),
(28, 86),
(28, 88),
(28, 91),
(28, 102),
(28, 107),
(28, 123),
(28, 125),
(28, 127),
(28, 128),
(28, 130),
(26, 100),
(26, 117),
(26, 121),
(26, 112),
(26, 102),
(26, 106),
(27, 102),
(27, 87),
(27, 112),
(27, 121),
(27, 117),
(27, 100),
(27, 106),
(34, 98),
(34, 93),
(34, 96),
(34, 111),
(34, 92),
(34, 85),
(34, 88),
(34, 90),
(34, 101),
(34, 106),
(34, 122),
(34, 124),
(34, 126),
(34, 128),
(34, 130),
(35, 130),
(35, 128),
(35, 126),
(35, 125),
(35, 122),
(35, 107),
(35, 102),
(35, 91),
(35, 88),
(35, 86),
(35, 95),
(35, 112),
(35, 121),
(35, 117),
(35, 100);

-- --------------------------------------------------------

--
-- Table structure for table `kriteria`
--

CREATE TABLE IF NOT EXISTS `kriteria` (
  `id_kriteria` int(11) NOT NULL AUTO_INCREMENT,
  `nm_kriteria` varchar(50) COLLATE latin1_general_ci NOT NULL,
  `atribut` enum('benefit','cost') COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`id_kriteria`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=40 ;

--
-- Dumping data for table `kriteria`
--

INSERT INTO `kriteria` (`id_kriteria`, `nm_kriteria`, `atribut`) VALUES
(31, 'Kehadiran', 'benefit'),
(29, 'Kerapihan', 'benefit'),
(30, 'Penyampaian Materi', 'benefit'),
(34, 'Ketepatan Waktu', 'benefit'),
(28, 'Membuat buku pada bidang sendiri', 'benefit'),
(25, 'Pendidikan', 'benefit'),
(26, 'Sedang/ pernah melakukan penelitian', 'benefit'),
(27, 'Pernah menjabat pada lembaga pemerintahan', 'benefit'),
(32, 'Kepuasan Mahasiswa', 'benefit'),
(33, 'Penguasaan Materi', 'benefit'),
(35, 'Membuat Book Chapter', 'benefit'),
(36, 'Membuat penelitian ke sebuah jurnal', 'benefit'),
(37, 'Menyadur buku ilmiah', 'benefit'),
(38, 'Memberikan penyuluhan kepada masyarakat', 'benefit'),
(39, 'Melaksanakan pengembangan hasil pendidikan & penel', 'benefit');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `username` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` varchar(10) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`username`, `password`, `level`) VALUES
('admin', 'admin', 'Admin'),
('ahmad', 'ahmad', 'Dosen');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
