<div class="float-right">
   <?php
// menentukan jumlah halaman yang muncul berdasarkan jumlah semua data
   $jumhal = ceil($jmldata/$dataPerhal);

// menampilkan link previous

   if ($nohal > 1) echo  "<div class='button-group'><a href='".$link."&hal=".($nohal-1)."' title='First page' class='button blue-gradient glossy'><span class='icon-previous'></span></a></a></span>
</div>
";

// memunculkan nomor halaman dan linknya
echo '<div class="button-group">';
for($hal = 1; $hal <= $jumhal; $hal++)
{
   if ((($hal >= $nohal - 3) && ($hal <= $nohal + 3)) || ($hal == 1) || ($hal == $jumhal))
   {
      if (($showhal == 1) && ($hal != 2))  echo "...";
      if (($showhal != ($jumhal - 1)) && ($hal == $jumhal))  echo "...";
      if ($hal == $nohal) echo '
         
         <a href="#" title="Page '.$hal.'" class="button blue-gradient glossy active">'.$hal.'</a>
      ';
      else echo " <a href='".$link."&hal=".$hal."' title='Page ".$hal."' class='button blue-gradient glossy'>".$hal."</a> ";
      $showhal = $hal;
   }
}
echo '</div>';
// menampilkan link next

if ($nohal < $jumhal) echo "
   <div class='button-group'>
      <a href='".$link."&hal=".($nohal+1)."' title='Next page' class='button blue-gradient glossy'><span class='icon-next'></span></a>
      
   </div>";

   ?>
</div>