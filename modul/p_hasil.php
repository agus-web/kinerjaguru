<?php
if(empty($_SESSION['ses_login'])){
	exit("<script>location.href='./';</script>");
}

if(!empty($_POST['cmd_new'])){
	session_unregister('ANALISA_KRITERIA');
	exit("<script>location.href='?page=p_analisa';</script>");
}

# baca jumlah kriteria
$jumlah_kriteria=mysql_num_rows(mysql_query("select * from kriteria"));
# baca jumlah alternatif
$jumlah_alternatif=mysql_num_rows(mysql_query("select * from alternatif"));

# baca data alternatif
$q=mysql_query("select * from alternatif order by nm_guru");
while($h=mysql_fetch_array($q)){
	$alternatif[]=array($h['id_alternatif'],$h['nidn'],$h['nm_guru']);
	$title.='<td align="center" width="240">'.strtoupper($h['nm_guru']).'</td>';
}

# baca data kriteria dan nilai bobot dari form input analisa
$q=mysql_query("select * from kriteria");
while($h=mysql_fetch_array($q)){
	$nilai=$_SESSION['ANALISA_KRITERIA'][$h['id_kriteria']];
	$kriteria[]=array($h['id_kriteria'],$h['nm_kriteria'],$h['atribut'],$nilai);
}

$no=0;
$daftar='<td width="40"><b>NO</b></td><td width="100"><b>NIS</b></td><td><b>NAMA</b></td>';
for($i=0;$i<count($kriteria);$i++){
	$daftar.='<td width="200"><b>C'.($i+1).'</b></td>';
}
$daftar='<tr>'.$daftar.'</tr>';
for($i=0;$i<count($alternatif);$i++){
	$no++;
	$daftar.='<tr><td>'.$no.'</td><td>'.$alternatif[$i][1].'</td><td>'.$alternatif[$i][2].'</td>';
	for($ii=0;$ii<count($kriteria);$ii++){
		$q=mysql_query("select himpunan.nm_himpunan from klasifikasi inner join himpunan on klasifikasi.id_himpunan=himpunan.id_himpunan where klasifikasi.id_alternatif='".$alternatif[$i][0]."' and himpunan.id_kriteria='".$kriteria[$ii][0]."'");
		$h=mysql_fetch_array($q);
		$himpunan=$h['nm_himpunan'];
		$daftar.='<td>'.$himpunan.'</td>';
	}
	$daftar.='</tr>';
}

$no=0;
$daftar_1='<td width="40"><b>NO</b></td><td width="100"><b>NIS</b></td><td><b>NAMA</b></td>';
for($i=0;$i<count($kriteria);$i++){
	$daftar_1.='<td width="60"><b>C'.($i+1).'</b></td>';
}
$daftar_1='<tr>'.$daftar_1.'</tr>';
for($i=0;$i<count($alternatif);$i++){
	$no++;
	$daftar_1.='<tr><td>'.$no.'</td><td>'.$alternatif[$i][1].'</td><td>'.$alternatif[$i][2].'</td>';
	for($ii=0;$ii<count($kriteria);$ii++){
		$q=mysql_query("select himpunan.nilai from klasifikasi inner join himpunan on klasifikasi.id_himpunan=himpunan.id_himpunan where klasifikasi.id_alternatif='".$alternatif[$i][0]."' and himpunan.id_kriteria='".$kriteria[$ii][0]."'");
		$h=mysql_fetch_array($q);
		$nilai=$h['nilai'];
		# catat nilai himpunan ke dalam matriks
		$matriks_x[$i+1][$ii+1]=$nilai;
		$daftar_1.='<td>'.$nilai.'</td>';
	}
	$daftar_1.='</tr>';
}

# NORMALISASI 1
$no=0;
$daftar_2='<td width="40"><b>NO</b></td><td width="100"><b>NIS/NIDN</b></td><td><b>NAMA</b></td>';
for($i=0;$i<count($kriteria);$i++){
	$daftar_2.='<td width="60"><b>C'.($i+1).'</b></td>';
}
$daftar_2='<tr>'.$daftar_2.'</tr>';
for($i=0;$i<count($alternatif);$i++){
	$no++;
	$daftar_2.='<tr><td>'.$no.'</td><td>'.$alternatif[$i][1].'</td><td>'.$alternatif[$i][2].'</td>';
	for($ii=0;$ii<count($kriteria);$ii++){
		$arr='';
		for($j=0;$j<count($alternatif);$j++){ # alternatif
			$arr[]=$matriks_x[$j+1][$ii+1];
		}
		if($kriteria[$ii][2]=='benefit'){
			if($matriks_x[$i+1][$ii+1]>0){$jml=$matriks_x[$i+1][$ii+1]/max($arr);}else{$jml=0;}
		}else{
			if(min($arr)>0){$jml=min($arr)/$matriks_x[$i+1][$ii+1];}else{$jml=0;}
		}
		$matriks_1[$i+1][$ii+1]=round($jml,3);
		$daftar_2.='<td>'.round($jml,3).'</td>';
	}
	$daftar_2.='</tr>';
}

// NORMALISASI 2
for($i=0;$i<count($alternatif);$i++){
	$jml=0;
	for($ii=0;$ii<count($kriteria);$ii++){
		$jml=$jml + ($kriteria[$ii][3]*$matriks_1[$i+1][$ii+1]);
	}
	$hasil[]=array(round($jml,3),$alternatif[$i][0]);
}
sort($hasil);
//for($i=0;$i<count($hasil);$i++){
for($i=count($hasil)-1;$i>=0;$i--){
	$rank=count($hasil)-$i;
	$hasil_akhir[$hasil[$i][1]]=array($hasil[$i][0],$rank);
	if(empty($best_alternatif)){
		$q=mysql_query("select * from alternatif where id_alternatif='".$hasil[$i][1]."'");
		$h=mysql_fetch_array($q);
		$nm_guru=$h['nm_guru'];
		$best_alternatif=$nm_guru;
	}
}

$no=0;
$daftar_3='<td align="center" width="40"><b>NO</b></td><td align="center" width="100"><b>NIS</b></td><td align="center"><b>NAMA</b></td><td align="center" width="100"><b>NILAI</b></td><td align="center" width="100"><b>RANK</b></td>';
$daftar_3='<tr>'.$daftar_3.'</tr>';
for($i=0;$i<count($alternatif);$i++){
	$no++;
	$daftar_3.='<tr><td>'.$no.'</td><td>'.$alternatif[$i][1].'</td><td>'.$alternatif[$i][2].'</td><td>'.$hasil_akhir[$alternatif[$i][0]][0].'</td><td>'.$hasil_akhir[$alternatif[$i][0]][1].'</td></tr>';
	//$daftar_3.='<tr><td>'.$no.'</td></tr>';
}
?>

        <div style="font-family:Arial;font-size:15px;padding:3px ">
		<br>
		<!--<div style="overflow:scroll;height:520px;">-->
		<div style="overflow:scroll:width:400px;">
		<table width="<?php echo (340+($jumlah_kriteria*180));?>" border="2" cellspacing="4" cellpadding="0" class="table table-striped table-hover table-bordered">
		  <?php echo $daftar;?>
		</table>
		</div>
		<br /><br />
		<div style="overflow:scroll">
		<table width="<?php echo (340+($jumlah_kriteria*60));?>" border="2" cellspacing="4" cellpadding="0" class="table table-striped table-hover table-bordered">
		  <?php echo $daftar_1;?>
		</table>
		</div>
		<br /><br /><b>NORMALISASI</b><br /><br />
		<div style="overflow:scroll">
		<table width="<?php echo (340+($jumlah_kriteria*60));?>" border="2" cellspacing="4" cellpadding="0" class="table table-striped table-hover table-bordered">
		  <?php echo $daftar_2;?>
		</table>
		</div>
		<br /><br />
		<div>
		<table width="100%" border="2" cellspacing="4" cellpadding="0" class="table table-striped table-hover table-bordered">
		  <?php echo $daftar_3;?>
		</table>
		</div>
		<!--</div>-->
    	</div>
